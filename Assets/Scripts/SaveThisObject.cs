﻿using UnityEngine;
using System.Collections;

public class SaveThisObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(gameObject);
		Destroy (this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
