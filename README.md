# ***README*** #

Everybody should make their own dungeon / level / whatever

For In-depth information, go read [docs/documentation.md](https://bitbucket.org/wgdc/unity-rpg/src/1f54a493f414341af1c3cc75356b66fbfbccd1fb/docs/documentation.md?at=master)

### Your Level Will Need ###
* Map
    * Terrain or you can create hallways and rooms
* Weapons / Tools / etc
    * Extend "Holdable" and fill out the activate() method
    * Don't forget to tag it as "Holdable" and add a rigidbody
    * Make sure to give your player a "Hand"
* Models and Textures
    * Try searching around online or make your own

### Overall Important Things to Remember ###
* Make your own branch for features you want to add
* Always keep your project organized / well-named
* Make a sub-folder for your level in the main project
* Remember to also write documentation for scripts you write
* When in doubt, make a new script - they're free!
* For best practices on completing assignments look at docs/development.md

### Eventually You'll be Adding ###
* Quests!
* Enemies!
* Connections Back to the Overworld!